import requests
import json
import chardet
import xmltodict
import codecs


# https://data.gov.ru/pravila-i-rekomendacii

# Мега-Учебник Flask Глава 1: Привет, мир! ( издание 2018 )
# https://habr.com/post/346306/

def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class DataGovService:
    url = 'https://data.gov.ru/api/'
    access_token = '99a4b8fe8c08ea648fe72897b8e51225'

    @staticmethod
    def encode_text(text):
        result = text
        try:
            result = bytes(text, encoding='iso-8859-1').decode('utf-8')
        finally:
            return result

    @staticmethod
    def parse_csv_line(line):
        line = line.replace('","', ';').replace('",', ';').replace(',"', ';')
        if line[0] == '"':
            line = line[1:]
        if line[-1] == '"':
            line = line[:-1]
        return line.split(';')

    def get_dataset_info(self, dataset_id):
        response = requests.get('{0}dataset/{1}/?access_token={2}'.format(self.url, dataset_id, self.access_token))
        return json.loads(response.text)

    def get_dataset_versions(self, dataset_id):
        response = requests.get(
            '{0}dataset/{1}/version/?access_token={2}'.format(self.url, dataset_id, self.access_token))
        return json.loads(response.text)

    def get_dataset(self, dataset_id, version_id):
        response = requests.get(
            '{0}dataset/{1}/version/{2}/?access_token={3}'.format(self.url, dataset_id, version_id, self.access_token))
        return json.loads(response.text)

    @staticmethod
    def get_source(source_path):
        response = requests.get(source_path)
        return response.text

    def get_data_csv(self, items):
        lines = self.get_source(items[0]['source']).split('\r\n')
        # if len(items) <= 1:
        #     print('dataset fields is empty')
        #     return result
        key_items = self.parse_csv_line(lines[0])
        keys = {}
        for i in range(len(key_items)):
            keys[str(i)] = self.encode_text(key_items[i])
        data = []
        for i in range(1, len(lines)):
            if len(lines[i]) == 0:
                continue
            obj = {}
            fields = self.parse_csv_line(lines[i])
            if len(fields) < len(key_items):
                continue
            for j in range(len(key_items)):
                obj[str(j)] = self.encode_text(fields[j])
            data.append(obj)
        return keys, data

    def get_data(self, dataset_id):
        result = {}
        info = self.get_dataset_info(dataset_id)
        result['id'] = dataset_id
        result['title'] = info['title']
        result['description'] = info['description']
        result['subject'] = info['subject']
        result['creator'] = info['creator']
        result['format'] = info['format']
        result['keys'] = {}
        versions = self.get_dataset_versions(dataset_id)
        if len(versions) == 0:
            print('versions is empty')
            return result
        version = versions[0]['created']
        items = self.get_dataset(dataset_id, version)
        if len(items) == 0:
            print('dataset is empty')
            return result
        if result['format'] == 'csv':
            result['keys'], result['data'] = self.get_data_csv(items)
        elif result['format'] == 'json':
            result['data'] = self.get_source(items[0]['source'])
        elif result['format'] == 'xml':
            result['data'] = json.dumps(xmltodict.parse(self.get_source(items[0]['source'])))
        else:
            result['data'] = []
        print('all is ok')
        return result

    def get_data_all(self, page_index, all_pages_count):
        response = requests.get('{0}dataset/?access_token={1}'.format(self.url, self.access_token))
        data = json.loads(response.text)
        # TODO: optimize
        if len(data) > page_index * all_pages_count:
            data = data[(page_index - 1) * all_pages_count:page_index * all_pages_count]
        return data

    def get_data_by_ids(self, ids):
        data = []
        for identifier in ids:
            item = self.get_data(ids[identifier])
            result = dict()
            result['id'] = ids[identifier]
            result['title'] = item['title']
            result['organization'] = ids[identifier].split('-')[0]
            result['organization_name'] = item['subject']
            result['topic'] = ''
            data.append(result)
        return data

