import requests
import json


class VKService:
    VK_URL = 'https://api.vk.com/method/'
    VK_VERSION_API = '5.80'
    SERVICE_KEY = '3356b2963356b2963356b296cf3333e717333563356b2966800b8195c888eef1afb420a'
    USERS_GET_PARAMS = 'sex,bdate,city,country,home_town,has_photo,photo_200,online,domain,has_mobile,contacts,site,education,universities,schools,status,last_seen,followers_count,occupation,nickname,relatives,relation,personal,connections,exports,wall_comments,activities,interests,music,movies,tv,books,games,about,quotes,timezone,screen_name,maiden_name,career,military'
    SUBS_GET_PARAMS = 'name,screen_name,type,photo_200,city,contacts,country,description'
    FRIENDS_GET_PARAMS = 'nickname,domain,sex,bdate,city,country,timezone,photo_50,photo_100,photo_200_orig,has_mobile,contacts,education,online,relation,last_seen,status,can_write_private_message,can_see_all_posts,can_post,universities'

    def invoke(self, method_name, params):
        url = '{0}{1}?{2}&access_token={3}&v={4}'.format(self.VK_URL, method_name, params, self.SERVICE_KEY,
                                                         self.VK_VERSION_API)
        print(url)
        response = requests.get(url)
        return json.loads(response.text)

    def get_users_by_id(self, id):
        return self.invoke('users.get', 'user_ids={0}&fields={1}'.format(id, self.USERS_GET_PARAMS))

    def get_users_by_ids(self, ids):
        return self.invoke('users.get', 'user_ids={0}'.format(','.join(ids)))

    def get_user_groups(self, id):
        return self.invoke('groups.get', 'user_id={0}'.format(id))

    def get_user_subs(self, id):
        return self.invoke('users.getSubscriptions', 'user_id={0}&extended=1&fields={1}&count=200'.format(id, self.SUBS_GET_PARAMS))

    def get_user_friends(self, id):
        return self.invoke('friends.get', 'user_id={0}&fields={1}'.format(id, self.FRIENDS_GET_PARAMS))
