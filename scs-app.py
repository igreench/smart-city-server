import json

from bson import ObjectId
from flask import Flask, jsonify, request
from flask_cors import CORS

from datagovservice import DataGovService
from vkservice import VKService

# from pymongo import MongoClient

app = Flask(__name__, static_folder='.', static_url_path='')
cors = CORS(app)

uri = 'mongodb://root:hacker5qvadmlabpossible@ds163610.mlab.com:63610/data-gov'
dataset_id = '7710747640-roznvn'
# client = MongoClient(uri)
# db = client['data-gov']
headers = {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'}
data_service = DataGovService()
vk_service = VKService()


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def create_http_response(data):
    return data, 200, headers


@app.route('/')
def index():
    return 'It Works!'


# @app.route('/api/data_base/add')
# def add_data():
#     data_service = DataGovService()
#     data = data_service.get_data(dataset_id)
#     data_id = db.processed_instances.insert_one(data).inserted_id
#     return create_http_response(jsonify({'id': data_id}))
#
#
# @app.route('/api/data_base')
# def get_data():
#     cursor = db.processed_instances.find({})
#     items = []
#     for document in cursor:
#         items.append(document)
#     return create_http_response(JSONEncoder().encode(items))


@app.route('/api/data/all')
def get_data_all():
    page_index = request.args.get('pageIndex')
    all_pages_count = request.args.get('allPagesCount')
    data = data_service.get_data_all(int(page_index), int(all_pages_count))
    return create_http_response(JSONEncoder().encode(data))


@app.route('/api/data/by_ids')
def get_data_by_ids():
    ids = request.args
    data = data_service.get_data_by_ids(ids)
    return create_http_response(JSONEncoder().encode(data))


@app.route('/api/data')
def get_data_by_id():
    id = request.args.get('id')
    data = data_service.get_data(id)
    return create_http_response((jsonify(data)))


@app.route('/api/vk')
def get_vk():
    id = request.args.get('id')
    user_info = vk_service.get_users_by_id(id)
    subs_info = vk_service.get_user_subs(id)
    data = dict()
    data['user'] = user_info
    data['subs'] = subs_info
    return create_http_response(JSONEncoder().encode(data))


@app.route('/api/vk/group')
def get_vk_groups():
    id = request.args.get('id')
    data = vk_service.get_user_groups(id)
    return create_http_response(JSONEncoder().encode(data))


@app.route('/api/vk/subs')
def get_vk_subs():
    id = request.args.get('id')
    data = vk_service.get_user_subs(id)
    return create_http_response(JSONEncoder().encode(data))


@app.route('/api/vk/friends')
def get_vk_friends():
    id = request.args.get('id')
    data = vk_service.get_user_friends(id)
    return create_http_response(JSONEncoder().encode(data))


# start listening
if __name__ == '__main__':
    app.run(debug=True)
